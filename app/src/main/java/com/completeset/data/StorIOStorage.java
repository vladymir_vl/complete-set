package com.completeset.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.completeset.ui.entity.CPU;
import com.completeset.ui.entity.CPUSQLiteTypeMapping;
import com.completeset.ui.entity.GPU;
import com.completeset.ui.entity.GPUSQLiteTypeMapping;
import com.completeset.ui.entity.MemoryCard;
import com.completeset.ui.entity.MemoryCardSQLiteTypeMapping;
import com.completeset.ui.entity.Motherboard;
import com.completeset.ui.entity.MotherboardSQLiteTypeMapping;
import com.completeset.ui.entity.Vendor;
import com.completeset.ui.entity.VendorSQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

/**
 * Class for instantiate instance of StorIOSQLite
 *
 * @author Vladimir Vladykin
 */
public final class StorIOStorage {

    @NonNull
    public static StorIOSQLite newInstance(@NonNull Context context) {
        SqlHelper helper = new SqlHelper(context);
        helper.create();

        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(helper)
                .addTypeMapping(Vendor.class, new VendorSQLiteTypeMapping())
                .addTypeMapping(Motherboard.class, new MotherboardSQLiteTypeMapping())
                .addTypeMapping(CPU.class, new CPUSQLiteTypeMapping())
                .addTypeMapping(GPU.class, new GPUSQLiteTypeMapping())
                .addTypeMapping(MemoryCard.class, new MemoryCardSQLiteTypeMapping())
                .build();
    }

    private StorIOStorage() {
        // nobody can instantiate this class
        throw new UnsupportedOperationException();
    }
}
