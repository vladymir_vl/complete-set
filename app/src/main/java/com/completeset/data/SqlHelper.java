package com.completeset.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.completeset.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Class for help work with sql data base.
 */
public final class SqlHelper extends SQLiteOpenHelper {

    private static final String DATABASE_FOLDER = "databases";
    private static final String COMPLETE_SET_BASE_NAME = "complete_set.db";
    private static final int VERSION = 1;

    private Context context;

    public SqlHelper(Context context) {
        super(context, COMPLETE_SET_BASE_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public void create() {
        if (databaseExists()) {
            return;
        }

        try {
            copyDataBaseFromAssets(context);
        } catch (IOException exception) {
            exception.printStackTrace();
            Toast.makeText(context,  R.string.error_db_copy, Toast.LENGTH_LONG)
                    .show();
        }
    }

    private boolean databaseExists() {
        SQLiteDatabase database = null;

        String databasePath = context.getDatabasePath(COMPLETE_SET_BASE_NAME)
                .getAbsolutePath();

        try {
            database = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (Exception ignored) {
            // database doesn't exist yet
        }

        if (database != null) {
            database.close();
        }

        return database != null;

    }

    private void copyDataBaseFromAssets(Context context) throws IOException {
        File databaseTargetFile = context
                .getDatabasePath(COMPLETE_SET_BASE_NAME);

        File databaseDirectory = databaseTargetFile.getParentFile();
        if (!databaseDirectory.exists()) {
            boolean ignored = databaseDirectory.mkdirs();
        }

        InputStream input = context.getAssets().open(
                DATABASE_FOLDER + File.separator + COMPLETE_SET_BASE_NAME);
        OutputStream output = new FileOutputStream(databaseTargetFile);

        byte[] buffer = new byte[1024];
        int length;
        while ( (length = input.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        input.close();
    }
}
