package com.completeset.data.table;

/**
 * Columns for Motherboards table.
 *
 * @author Vladimir Vladykin
 */
public interface MotherboardTable extends Table {

    String TABLE = "motherboards";
    String NAME = "name";
    String VENDOR = "vendor";
    String MEMORY_TYPE = "memory_type";
    String SOCKET_TYPE = "socket_type";
    String PCI_E_TYPE = "pci_e_type";
    String PRICE = "price";
}
