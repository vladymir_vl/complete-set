package com.completeset.data.table;

/**
 * Table for GPU.
 */
public interface GPUTable extends Table {

    String TABLE = "video_cards";
    String NAME = "name";
    String PCI_E_TYPE = "pci_e_type";
    String PRICE = "price";
}
