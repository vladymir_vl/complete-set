package com.completeset.data.table;

/**
 * Table for CPU.
 */
public interface CPUTable extends Table {

    String TABLE = "processors";
    String NAME = "name";
    String SOCKET_TYPE = "socket_type";
    String VENDOR = "vendor";
    String PRICE = "price";
}
