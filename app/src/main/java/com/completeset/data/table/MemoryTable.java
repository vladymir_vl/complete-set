package com.completeset.data.table;

/**
 * Table for memory cards.
 */
public interface MemoryTable extends Table {

    String TABLE = "memory_cards";
    String NAME = "name";
    String TYPE = "type";
    String PRICE = "price";
}
