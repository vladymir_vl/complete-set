package com.completeset.data.table;

/**
 * Base interface for tables.
 */
public interface Table {

    String ID = "_id";

}
