package com.completeset.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.completeset.R;
import com.completeset.ui.entity.OrderItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for show list of OrderItems.
 */
public final class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.OrderItemHolder> {

    private List<OrderItem> items;

    public OrderItemsAdapter(List<OrderItem> items) {
        this.items = items;
    }

    @Override
    public OrderItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderItemHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public final class OrderItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_order_name) TextView name;
        @BindView(R.id.item_order_price) TextView price;

        public OrderItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(OrderItem orderItem) {
            name.setText(orderItem.getName());
            price.setText(String.valueOf(orderItem.getPrice()));
        }
    }
}
