package com.completeset.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.completeset.R;
import com.completeset.ui.entity.OrderItem;
import com.completeset.ui.fragment.CompleteSetFragment;
import com.completeset.ui.fragment.OrderFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity implements CompleteSetFragment.Callback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            startFragment(new CompleteSetFragment());
        }
    }

    @Override
    public void onSetCompleted(List<OrderItem> items) {
        startFragment(OrderFragment.newInstance(items));
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
            return;
        }

        super.onBackPressed();
    }

    private void startFragment(Fragment fragment) {
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }
}
