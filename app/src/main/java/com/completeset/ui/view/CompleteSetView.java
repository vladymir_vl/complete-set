package com.completeset.ui.view;

import android.support.annotation.Nullable;

import com.completeset.ui.entity.CPU;
import com.completeset.ui.entity.GPU;
import com.completeset.ui.entity.MemoryCard;
import com.completeset.ui.entity.Motherboard;
import com.completeset.ui.entity.Vendor;

import java.util.List;

/**
 * View for let user choose PC complete set.
 */
public interface CompleteSetView {

    void setVendors(List<Vendor> vendors);
    void setMotherboards(@Nullable List<Motherboard> motherboards);
    void setProcessors(@Nullable List<CPU> processors);
    void setVideoCards(@Nullable List<GPU> videoCards);
    void setMemoryCards(@Nullable List<MemoryCard> memoryCards);

    void showMessage(String message);
}
