package com.completeset.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.completeset.R;
import com.completeset.data.StorIOStorage;
import com.completeset.ui.SimpleItemSelectedListener;
import com.completeset.ui.entity.CPU;
import com.completeset.ui.entity.OrderItem;
import com.completeset.ui.entity.GPU;
import com.completeset.ui.entity.MemoryCard;
import com.completeset.ui.entity.Motherboard;
import com.completeset.ui.entity.Vendor;
import com.completeset.ui.presenter.CompleteSetPresenter;
import com.completeset.ui.view.CompleteSetView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment for let user choose complete set.
 */
public class CompleteSetFragment extends Fragment implements CompleteSetView {

    @BindView(R.id.complete_set_vendor_spinner) Spinner vendorSpinner;
    @BindView(R.id.complete_set_motherboard_spinner) Spinner motherboardSpinner;
    @BindView(R.id.complete_set_cpu_spinner) Spinner cpuSpinner;
    @BindView(R.id.complete_set_gpu_spinner) Spinner gpuSpinner;
    @BindView(R.id.complete_set_memory_spinner) Spinner memorySpinner;

    private List<Vendor> vendors;
    private List<Motherboard> motherboards;
    private List<CPU> processors;
    private List<GPU> videoCards;
    private List<MemoryCard> memoryCards;

    private CompleteSetPresenter presenter;
    private Unbinder unbinder;
    private Callback callback;

    @BindString(R.string.complete_set_default_hint) String defaultHint;

    public interface Callback {
        void onSetCompleted(List<OrderItem> items);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (Callback) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        presenter = new CompleteSetPresenter(StorIOStorage.newInstance(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complete_set, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);

        setSpinnerAdapter(vendorSpinner, null);
        setSpinnerAdapter(motherboardSpinner, null);
        setSpinnerAdapter(cpuSpinner, null);
        setSpinnerAdapter(gpuSpinner, null);
        setSpinnerAdapter(memorySpinner, null);
        establishItemSelectedListeners();

        presenter.bindView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.unbindView(this);
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_order, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_order) {
            dispatchOrdering();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
        setSpinnerAdapter(vendorSpinner, formatDisplayableArray(new ArrayList<>(vendors)));
    }

    @Override
    public void setMotherboards(@Nullable List<Motherboard> motherboards) {
        this.motherboards = motherboards;
        setSpinnerAdapter(motherboardSpinner, motherboards != null ?
                formatDisplayableArray(new ArrayList<>(motherboards)) : null
        );
    }

    @Override
    public void setProcessors(@Nullable List<CPU> processors) {
        this.processors = processors;
        setSpinnerAdapter(cpuSpinner, processors != null ?
                formatDisplayableArray(new ArrayList<>(processors)) : null
        );
    }

    @Override
    public void setVideoCards(@Nullable List<GPU> videoCards) {
        this.videoCards = videoCards;
        setSpinnerAdapter(gpuSpinner, videoCards != null ?
                formatDisplayableArray(new ArrayList<>(videoCards)) : null
        );
    }

    @Override
    public void setMemoryCards(@Nullable List<MemoryCard> memoryCards) {
        this.memoryCards = memoryCards;
        setSpinnerAdapter(memorySpinner, memoryCards != null ?
                formatDisplayableArray(new ArrayList<>(memoryCards)) : null
        );
    }

    private void setSpinnerAdapter(Spinner spinner,
                                   @Nullable String[] items) {
        if (items == null) {
            items = new String[1];
            items[0] = defaultHint;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private String[] formatDisplayableArray(List<OrderItem> orderItemList) {
        String[] array = new String[orderItemList.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = orderItemList.get(i).getName();
        }

        return array;
    }

    private void dispatchOrdering() {
        List<OrderItem> orderItems = new ArrayList<>();

        // we don't include vendor to order
        List<Spinner> spinners = Arrays.asList(
                motherboardSpinner, cpuSpinner, gpuSpinner, memorySpinner);
        List<List> arrays = Arrays.asList(
                motherboards, processors, videoCards, memoryCards);

        for (int i = 0; i < spinners.size(); i++) {
            View spinnerSelectedView = spinners.get(i).getSelectedView();
            if (clickOnDefaultItem(spinnerSelectedView)) {
                continue;
            }

            int spinnerSelectedPosition = spinners.get(i).getSelectedItemPosition();
            OrderItem orderItem = (OrderItem)  arrays.get(i).get(spinnerSelectedPosition);
            orderItems.add(orderItem);
        }

        callback.onSetCompleted(orderItems);
    }

    private void establishItemSelectedListeners() {
        vendorSpinner.setOnItemSelectedListener(new SimpleItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (clickOnDefaultItem(view)) {
                    // click on default item, nothing to do
                    return;
                }

                presenter.onVendorSelected(position);
            }
        });

        motherboardSpinner.setOnItemSelectedListener(new SimpleItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (clickOnDefaultItem(view)) {
                    // click on default item, nothing to do
                    return;
                }

                presenter.onMotherboardSelected(position);
            }
        });
    }

    private boolean clickOnDefaultItem(@Nullable View view) {
        if (view == null) {
            return false;
        }
        TextView textView = (TextView) view;
        return textView.getText().toString().equals(defaultHint);
    }
}
