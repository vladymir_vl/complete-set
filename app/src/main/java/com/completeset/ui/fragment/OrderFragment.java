package com.completeset.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.completeset.R;
import com.completeset.ui.SerializableOrderItems;
import com.completeset.ui.adapter.OrderItemsAdapter;
import com.completeset.ui.entity.OrderItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Fragment for make order.
 */
public class OrderFragment extends Fragment {

    private static final String ORDER_ITEMS = "order_items";

    private List<OrderItem> orderItems;
    private OrderItemsAdapter adapter;
    private Unbinder unbinder;

    @BindView(R.id.order_recycler_view) RecyclerView orderItemsList;
    @BindView(R.id.item_order_name) TextView amountTextView;
    @BindView(R.id.item_order_price) TextView priceTextView;

    public static OrderFragment newInstance(List<OrderItem> orderItems) {
        Bundle args = new Bundle();
        args.putSerializable(ORDER_ITEMS, new SerializableOrderItems(orderItems));
        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SerializableOrderItems serializableOrderItems = (SerializableOrderItems) getArguments()
                .getSerializable(ORDER_ITEMS);
        assert serializableOrderItems != null;
        orderItems = serializableOrderItems.getOrderItems();

        adapter = new OrderItemsAdapter(orderItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);

        amountTextView.setText(R.string.order_amount);
        setupOrderList();
        calculateAmount();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.order_make_button)
    void onOrderClick() {
        sendEmailWithItems(orderItems);
    }

    private void setupOrderList() {
        orderItemsList.setHasFixedSize(true);
        orderItemsList.setLayoutManager(new LinearLayoutManager(getContext()));
        orderItemsList.setAdapter(adapter);
    }

    private void calculateAmount() {
        int amount = 0;

        for (OrderItem item : orderItems) {
            amount += item.getPrice();
        }

        priceTextView.setText(String.valueOf(amount));
    }

    private void sendEmailWithItems(List<OrderItem> orderItems) {
        String emailText = createEmailTextFromItems(orderItems);

        Intent emailIntent = new Intent(Intent.ACTION_SEND)
                .setType("text/html")
                .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_title))
                .putExtra(Intent.EXTRA_TEXT, emailText);

        startActivity(emailIntent);
    }

    private String createEmailTextFromItems(List<OrderItem> orderItems) {
        StringBuilder stringBuilder = new StringBuilder();

        for (OrderItem item : orderItems) {
            stringBuilder.append(item.getName());
            stringBuilder.append("\t\t");
            stringBuilder.append(item.getPrice());
            stringBuilder.append("\n");
        }

        stringBuilder.append("\n");
        stringBuilder.append(getString(R.string.order_amount));
        stringBuilder.append("\t\t");
        stringBuilder.append(priceTextView.getText());

        return stringBuilder.toString();
    }
}
