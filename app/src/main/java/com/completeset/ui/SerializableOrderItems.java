package com.completeset.ui;

import com.completeset.ui.entity.OrderItem;

import java.io.Serializable;
import java.util.List;

/**
 * Class for serialize List of OrderItem.
 */
public final class SerializableOrderItems implements Serializable {

    private final List<OrderItem> orderItems;

    public SerializableOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }
}
