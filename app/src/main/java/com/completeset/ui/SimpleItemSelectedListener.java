package com.completeset.ui;

import android.view.View;
import android.widget.AdapterView;

/**
 * OnItemSelectedListener with default methods implementations.
 *
 * @author Vladimir Vladykin
 */
public abstract class SimpleItemSelectedListener implements AdapterView.OnItemSelectedListener {

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
