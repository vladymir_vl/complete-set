package com.completeset.ui.presenter;

import android.support.annotation.NonNull;

import com.completeset.data.table.CPUTable;
import com.completeset.data.table.GPUTable;
import com.completeset.data.table.MemoryTable;
import com.completeset.data.table.MotherboardTable;
import com.completeset.data.table.VendorsTable;
import com.completeset.ui.entity.CPU;
import com.completeset.ui.entity.GPU;
import com.completeset.ui.entity.MemoryCard;
import com.completeset.ui.entity.Motherboard;
import com.completeset.ui.entity.Vendor;
import com.completeset.ui.view.CompleteSetView;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.io.IOException;
import java.util.List;

import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter for interact with CompleteSetView.
 */
public final class CompleteSetPresenter extends Presenter<CompleteSetView> {

    @NonNull private StorIOSQLite storage;

    private List<Vendor> vendors;
    private List<Motherboard> motherboards;

    public CompleteSetPresenter(@NonNull StorIOSQLite storage) {
        this.storage = storage;
    }

    @Override
    public void bindView(@NonNull CompleteSetView view) {
        super.bindView(view);
        loadVendorsIfNeeded();
    }

    public void onDestroy() {
        try {
            storage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onVendorSelected(int position) {
        if (vendors == null) {
            return;
        }
        loadMotherboardsByVendor(vendors.get(position));
    }

    public void onMotherboardSelected(int position) {
        if (motherboards == null) {
            return;
        }
        loadSetByMotherboard(motherboards.get(position));
    }

    private void loadVendorsIfNeeded() {
        if (vendors == null) {
            Subscription subscription = storage.get()
                    .listOfObjects(Vendor.class)
                    .withQuery(Query.builder()
                            .table(VendorsTable.TABLE)
                            .build())
                    .prepare()
                    .asRxSingle()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            loadedVendors -> {
                                vendors = loadedVendors;
                                view().setVendors(vendors);
                            },
                            throwable -> {
                                throwable.printStackTrace();
                                view().showMessage(throwable.getMessage());
                            }
                    );

            unsubcribeAfterUnbind(subscription);
        } else {
            view().setVendors(vendors);
        }
    }

    private void loadMotherboardsByVendor(@NonNull Vendor vendor) {
        Subscription subscription = storage.get()
                .listOfObjects(Motherboard.class)
                .withQuery(Query.builder()
                        .table(MotherboardTable.TABLE)
                        .where(MotherboardTable.VENDOR + " = ?")
                        .whereArgs(vendor.getId())
                        .build())
                .prepare()
                .asRxSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        loadedMotherboards -> {
                            motherboards = loadedMotherboards;
                            view().setMotherboards(motherboards);
                        },
                        throwable -> {
                            throwable.printStackTrace();
                            view().showMessage(throwable.getMessage());
                        }
                );

        unsubcribeAfterUnbind(subscription);
    }

    private final class CompleteSet {
        final List<CPU> cpuList;
        final List<GPU> gpuList;
        final List<MemoryCard> memoryCardList;

        public CompleteSet(List<CPU> cpuList, List<GPU> gpuList, List<MemoryCard> memoryCardList) {
            this.cpuList = cpuList;
            this.gpuList = gpuList;
            this.memoryCardList = memoryCardList;
        }
    }

    private void loadSetByMotherboard(@NonNull Motherboard motherboard) {
        // three query parallel
        Subscription subscription =
                Single.zip(
                        storage.get().listOfObjects(CPU.class)
                                .withQuery(Query.builder()
                                        .table(CPUTable.TABLE)
                                        .where(CPUTable.SOCKET_TYPE + " = ? AND " + CPUTable.VENDOR + " = ?")
                                        .whereArgs(motherboard.getSocketTypeId(), motherboard.getVendorId())
                                        .build())
                                .prepare().asRxSingle(),
                        storage.get().listOfObjects(GPU.class)
                                .withQuery(Query.builder()
                                        .table(GPUTable.TABLE)
                                        .where(GPUTable.PCI_E_TYPE + " = ?")
                                        .whereArgs(motherboard.getVideoCardSocketTypeId())
                                        .build())
                                .prepare().asRxSingle(),
                        storage.get().listOfObjects(MemoryCard.class)
                                .withQuery(Query.builder()
                                        .table(MemoryTable.TABLE)
                                        .where(MemoryTable.TYPE + " = ?")
                                        .whereArgs(motherboard.getMemoryTypeId())
                                        .build())
                                .prepare().asRxSingle(),
                        CompleteSet::new)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                completeSet -> {
                                    view().setProcessors(completeSet.cpuList);
                                    view().setVideoCards(completeSet.gpuList);
                                    view().setMemoryCards(completeSet.memoryCardList);
                                },
                                throwable -> {
                                    throwable.printStackTrace();
                                    view().showMessage(throwable.getMessage());
                                }
                        );

        unsubcribeAfterUnbind(subscription);
    }
}
