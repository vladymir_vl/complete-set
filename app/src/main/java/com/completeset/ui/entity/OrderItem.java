package com.completeset.ui.entity;

import java.io.Serializable;

/**
 * This object can be shown in Adapter.
 *
 * @author Vladimir Vladykin
 */
public interface OrderItem extends Serializable {

    String getName();
    int getPrice();
}
