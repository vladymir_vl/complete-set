package com.completeset.ui.entity;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static com.completeset.data.table.MotherboardTable.ID;
import static com.completeset.data.table.MotherboardTable.MEMORY_TYPE;
import static com.completeset.data.table.MotherboardTable.NAME;
import static com.completeset.data.table.MotherboardTable.PCI_E_TYPE;
import static com.completeset.data.table.MotherboardTable.PRICE;
import static com.completeset.data.table.MotherboardTable.SOCKET_TYPE;
import static com.completeset.data.table.MotherboardTable.TABLE;
import static com.completeset.data.table.MotherboardTable.VENDOR;

/**
 * DB entity for motherboards.
 *
 * @author Vladimir Vladykin
 */
@StorIOSQLiteType(table = TABLE)
public final class Motherboard implements OrderItem {

    @StorIOSQLiteColumn(name = ID, key = true) Long id;
    @StorIOSQLiteColumn(name = NAME) String name;
    @StorIOSQLiteColumn(name = VENDOR) int vendorId;
    @StorIOSQLiteColumn(name = MEMORY_TYPE) int memoryTypeId;
    @StorIOSQLiteColumn(name = SOCKET_TYPE) int socketTypeId;
    @StorIOSQLiteColumn(name = PCI_E_TYPE) int videoCardSocketTypeId;
    @StorIOSQLiteColumn(name = PRICE) int price;

    Motherboard() {}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public int getVendorId() {
        return vendorId;
    }

    public int getMemoryTypeId() {
        return memoryTypeId;
    }

    public int getSocketTypeId() {
        return socketTypeId;
    }

    public int getVideoCardSocketTypeId() {
        return videoCardSocketTypeId;
    }
}
