package com.completeset.ui.entity;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static com.completeset.data.table.MemoryTable.NAME;
import static com.completeset.data.table.MemoryTable.PRICE;
import static com.completeset.data.table.MemoryTable.TABLE;
import static com.completeset.data.table.MemoryTable.TYPE;
import static com.completeset.data.table.Table.ID;

/**
 * DB entity for memory cards.
 */
@StorIOSQLiteType(table = TABLE)
public final class MemoryCard implements OrderItem {

    @StorIOSQLiteColumn(name = ID, key = true) Long id;
    @StorIOSQLiteColumn(name = NAME) String name;
    @StorIOSQLiteColumn(name = TYPE) int memoryTypeId;
    @StorIOSQLiteColumn(name = PRICE) int price;

    MemoryCard() {}

    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public int getMemoryTypeId() {
        return memoryTypeId;
    }
}
