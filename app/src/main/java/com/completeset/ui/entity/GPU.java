package com.completeset.ui.entity;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static com.completeset.data.table.GPUTable.NAME;
import static com.completeset.data.table.GPUTable.PCI_E_TYPE;
import static com.completeset.data.table.GPUTable.PRICE;
import static com.completeset.data.table.GPUTable.TABLE;
import static com.completeset.data.table.Table.ID;

/**
 * DB entity for GPU.
 */
@StorIOSQLiteType(table = TABLE)
public final class GPU implements OrderItem {

    @StorIOSQLiteColumn(name = ID, key = true) Long id;
    @StorIOSQLiteColumn(name = NAME) String name;
    @StorIOSQLiteColumn(name = PCI_E_TYPE) int videoCardSocketTypeID;
    @StorIOSQLiteColumn(name = PRICE) int price;
    GPU() {}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public int getVideoCardSocketTypeID() {
        return videoCardSocketTypeID;
    }
}
