package com.completeset.ui.entity;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static com.completeset.data.table.Table.ID;
import static com.completeset.data.table.VendorsTable.NAME;
import static com.completeset.data.table.VendorsTable.TABLE;

/**
 * Entity for motherboard and cpu vendors.
 */
@StorIOSQLiteType(table = TABLE)
public final class Vendor implements OrderItem {

    @StorIOSQLiteColumn(name = ID, key = true) Long id;
    @StorIOSQLiteColumn(name = NAME) String name;

    Vendor() {}

    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        // vendor actually doesn't have price
        return 0;
    }
}
