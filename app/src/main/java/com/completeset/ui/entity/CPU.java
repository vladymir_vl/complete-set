package com.completeset.ui.entity;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static com.completeset.data.table.CPUTable.NAME;
import static com.completeset.data.table.CPUTable.PRICE;
import static com.completeset.data.table.CPUTable.SOCKET_TYPE;
import static com.completeset.data.table.CPUTable.TABLE;
import static com.completeset.data.table.CPUTable.VENDOR;
import static com.completeset.data.table.Table.ID;

/**
 * DB entity for CPU.
 */
@StorIOSQLiteType(table = TABLE)
public final class CPU implements OrderItem {

    @StorIOSQLiteColumn(name = ID, key = true) Long id;
    @StorIOSQLiteColumn(name = NAME) String name;
    @StorIOSQLiteColumn(name = SOCKET_TYPE) int socketTypeId;
    @StorIOSQLiteColumn(name = VENDOR) int vendorId;
    @StorIOSQLiteColumn(name = PRICE) int price;

    CPU() {}

    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public int getSocketTypeId() {
        return socketTypeId;
    }

    public int getVendorId() {
        return vendorId;
    }
}
